console.log('Hello, ')
// Details
const namee = {
    firstName: "wanYan",
    lName: "Namyam",
    age: 24,
    hobbies : [
        "Playing", "Eating", "Sleeping"
    ] ,
    addressAddress: {
        housenumber: "Block 1, Lot 1",
        street: "Japan, Cubao",
        city: "Volcano City",
        state: "Wills State",
    }
}
const work = Object.values(namee.addressAddress);
console.log("My First Name is " + namee.firstName)
console.log("My Last Name is " + namee.lName)
console.log(`Now, we put it together and my full name is ${namee.firstName} ${namee.lName}.`)
console.log("I am " + namee.age + " years old.")
console.log(`My 3 best hobbies are ${namee.hobbies.join(', ')}.`);
console.log("I am working at " + work.join(", ") + ".");